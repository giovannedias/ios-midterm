//
//  Created by giovanne emiliano
//  Student ID: 301044051
//  Copyright © 2020 Centennial College. All rights reserved.
//


import UIKit
import AVFoundation
import SpriteKit
import GameplayKit

let screenSize = UIScreen.main.bounds
var screenWidth: CGFloat?
var screenHeight: CGFloat?


class GameScene: SKScene {
    
    var gameManager: GameManager?
    
    var oceanSprite1: Ocean?
    var oceanSprite2: Ocean?
    var planeSprite: Plane?
    var islandSprite: Island?
    var cloudSprites: [Cloud] = []
    var cloud: Cloud = Cloud()

    
    var isLandscape = false
    
    //var config: Config?
    
    
    override func didMove(to view: SKView)
    {
        screenWidth = frame.width
        screenHeight = frame.height
        

        
        isLandscape = UIDevice.current.orientation.isLandscape
        
        //self.sceneState = .GAME
        //self.config?.sceneState = .GAME
        self.name = "GAME"
        
        // add ocean
        self.oceanSprite1 = Ocean()
        self.addChild(oceanSprite1!)
        
        self.oceanSprite2 = Ocean()
        self.addChild(oceanSprite2!)
        
        // add plane
        self.planeSprite = Plane()
        self.addChild(planeSprite!)
        
        // add island
        self.islandSprite = Island()
        self.addChild(islandSprite!)
        
        startWithViewOrientation()
        
        // add clouds
//        for index in 0...3
//        {
//            let cloud: Cloud = Cloud()
//            cloudSprites.append(cloud)
//            self.addChild(cloudSprites[index])
//        }
        
        self.addChild(cloud)
        
        let engineSound = SKAudioNode(fileNamed: "engine.mp3")
        self.addChild(engineSound)
        engineSound.autoplayLooped = true
        
        // preload sounds
        do {
            let sounds:[String] = ["thunder", "yay"]
            for sound in sounds
            {
                let path: String = Bundle.main.path(forResource: sound, ofType: "mp3")!
                let url: URL = URL(fileURLWithPath: path)
                let player: AVAudioPlayer = try AVAudioPlayer(contentsOf: url)
                player.prepareToPlay()
            }
        } catch {
        }
        
    }
    
    func startWithViewOrientation(){
        
        if isLandscape {
            print("isLandscape")
            
            
        
            self.oceanSprite1?.zRotation = -1 * (.pi / 2)
            self.oceanSprite2?.zRotation = -1 * (.pi / 2)

            self.planeSprite?.zRotation = -1 * (.pi / 2)
            self.planeSprite?.position = CGPoint(x: (-1 * self.frame.width/2) + 40, y: 0)
            planeSprite?.setScale(0.9)

            self.oceanSprite1?.position = CGPoint(x:  -1 * (self.frame.width/2) + 325, y: 0)
            self.oceanSprite2?.position = CGPoint(x: self.frame.width, y: 177)
            
            cloud.position = CGPoint(x: frame.width/2, y: 0)
            islandSprite?.position = CGPoint(x: 0, y: 0)
            cloud.setScale(0.5)
            islandSprite?.setScale(0.8)


        } else {
            print("isPortrait")
            planeSprite?.setScale(1.5)
            cloud.setScale(1.2)
            islandSprite?.setScale(1.5)

            self.oceanSprite1?.zRotation = 0
            self.oceanSprite2?.zRotation = 0
            
            self.planeSprite?.position = CGPoint(x: 0, y: -575)
            self.planeSprite?.zRotation = 0
            self.oceanSprite1?.position = CGPoint(x: 0, y: 1864.67)//self.frame.height)
            self.oceanSprite2?.position = CGPoint(x: 0, y: 177)

        }
        
        
        
    }
    
    
    func rotated() {
        
      
       // print("width: \(view?.frame.width) ocen1 x: \(oceanSprite1?.position.x)")
       let t  = UIDevice.current.orientation.isLandscape
        
        if t == isLandscape {
            // print("didnt change")
        } else {
            isLandscape = t
            print("change")
            startWithViewOrientation()
        }
        
      }
      
    
    func touch(atPoint pos : CGPoint){
        if isLandscape {
            self.planeSprite?.TouchMove(newPos: CGPoint(x:(-1 * self.frame.width/2) + 40, y: pos.y))

        } else {
            self.planeSprite?.TouchMove(newPos: CGPoint(x: pos.x, y: -575))

        }
    }
    
    func touchDown(atPoint pos : CGPoint)
    {
        touch(atPoint: pos  )
    }
    
    func touchMoved(toPoint pos : CGPoint)
    {
        touch(atPoint: pos )

        //self.planeSprite?.TouchMove(newPos: CGPoint(x: pos.x, y: -575))
    }
    
    func touchUp(atPoint pos : CGPoint)
    {
        touch(atPoint: pos )

        //self.planeSprite?.TouchMove(newPos: CGPoint(x: pos.x, y: -575))
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        for t in touches { self.touchDown(atPoint: t.location(in: self))}
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        for t in touches { self.touchMoved(toPoint: t.location(in: self))}
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        for t in touches { self.touchUp(atPoint: t.location(in: self))}
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        for t in touches { self.touchUp(atPoint: t.location(in: self))}
    }
    
    
    override func update(_ currentTime: TimeInterval)
    {
//        self.oceanSprite1?.Update()
//        self.oceanSprite2?.Update()
        
        self.oceanSprite1?.updateWithViewOrientation(isLandscape,viewFrame:  self.view?.frame)
        self.oceanSprite2?.updateWithViewOrientation(isLandscape,viewFrame:  self.view?.frame)
        
        self.planeSprite?.updateWithViewOrientation(isLandscape,viewFrame:  self.view?.frame)
        
        self.islandSprite?.updateWithViewOrientation(isLandscape,viewFrame:  self.view?.frame)
        
        cloud.updateWithViewOrientation(isLandscape, viewFrame: self.view?.frame)
        
        rotated()
        
//        for cloud in cloudSprites
//        {
//            cloud.Update()
//            CollisionManager.squaredRadiusCheck(scene: self, object1: planeSprite!, object2: cloud)
//        }

        CollisionManager.squaredRadiusCheck(scene: self, object1: planeSprite!, object2: cloud)


        CollisionManager.squaredRadiusCheck(scene: self, object1: planeSprite!, object2: islandSprite!)
          
        
          
          if(ScoreManager.Lives < 1)
          {
              self.gameManager?.PresentEndScene()
          }
        
        
        if ScoreManager.Score > 500 {
            self.oceanSprite1?.texture = SKTexture(imageNamed: "ocean-night")
            self.oceanSprite2?.texture = SKTexture(imageNamed: "ocean-night")

        } else {
            self.oceanSprite1?.texture = SKTexture(imageNamed: "ocean")
            self.oceanSprite2?.texture = SKTexture(imageNamed: "ocean")


        }
        return
        
            
        
        
    }
}
