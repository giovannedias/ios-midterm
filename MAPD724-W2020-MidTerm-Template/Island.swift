import SpriteKit
import GameplayKit

class Island: GameObject
{
    
    
    //constructor
    init()
    {
        super.init(imageString: "island", initialScale: 2.0)
        Start()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    // Life Cycle Functions
    override func CheckBounds()
    {
        // bottom boundary
        if(self.position.y < -715)
        {
            self.Reset()
        }
        
        // right boundary
        if(self.position.x >= 307 - self.halfWidth!)
        {
            self.position.x = 307 - self.halfWidth!
        }
        
        // left boundary
        if(self.position.x <= -307 + self.halfWidth!)
        {
            self.position.x = -307 + self.halfWidth!
        }
    }
    
    func Move()
    {
        self.position.y -= self.dy!
    }
    
    override func Reset()
    {
        self.position.y = 715
        let randomX:Int = (randomSource?.nextInt(upperBound: 614))! - 307
        self.position.x = CGFloat(randomX)
        self.isColliding = false
    }
    
    override func Start()
    {
        self.zPosition = 1
        self.Reset()
        self.dy = 5.0
    }
    
    override func Update()
    {
        self.Move()
        self.CheckBounds()
    }
    
    func updateWithViewOrientation(_ isLandscape: Bool, viewFrame : CGRect?) {
           
           guard let frame = viewFrame else {return}
           if isLandscape {
            
            self.position.x -= dy!
            
           // print("island x: \(self.position.x) frame: \(frame.width)")

            
            
            let max = frame.width/2


            if(self.position.x < -1 * max)
            {
                let min = -1 * frame.height/2
                let max = frame.height/2
                
                let randomY:Int =  Int.random(in: Int(min) ..< Int(max))//(randomSource?.nextInt(upperBound: Int(frame.height)/2))!
                self.position.y = CGFloat(randomY)
                self.position.x = frame.width/2
                self.isColliding = false

                return

            }

           } else {
               
            self.Move()
            self.CheckBounds()
              
           }
       }
}
