import SpriteKit
import GameplayKit

class Ocean : GameObject
{
    //constructor
    init()
    {
        super.init(imageString: "ocean", initialScale: 1.172)
        Start()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //GameObject Life Cycle Functions
    override func CheckBounds()
    {
           
        // check top boundary
        if(self.position.y <= -1505) // note maybe reset earlier
        {
            Reset()
        }
    }
    
    override func Reset()
    {
        self.position.y = 1864.67
    }
    
    override func Start()
    {
        self.zPosition = 0
        //self.Reset()
        self.dy = 5.0
    }
    func updateWithViewOrientation(_ isLandscape: Bool, viewFrame : CGRect?) {
        
        guard let frame = viewFrame else {return}
        if isLandscape {
            self.position.x -= self.dy!
            if(self.position.x <= -1 * frame.width) // note maybe reset earlier
            {
                self.position.x = frame.width
                
            }


        } else {
            self.position.y -= self.dy!
            if(self.position.y <= -1505) // note maybe reset earlier
            {
                self.position.y = 1864.67
                
            }

        }
       // CheckBounds()
    }
    
    override func Update() {
        Move()
        CheckBounds()
    }
    
    func Move()
    {
        self.position.y -= self.dy!
    }

}
