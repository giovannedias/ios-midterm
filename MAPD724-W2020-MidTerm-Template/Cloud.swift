import SpriteKit
import GameplayKit

class Cloud: GameObject
{
    
    
    //constructor
    init()
    {
        super.init(imageString: "cloud", initialScale: 1.5)
        Start()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    // Life Cycle Functions
    override func CheckBounds()
    {
        // bottom boundary
        if(self.position.y < -801)
        {
            self.Reset()
        }
        
    }
    
    func Move()
    {
        self.position.y -= self.dy!
        self.position.x -= self.dx!
    }
    
    override func Reset()
    {
        self.position.y = 801
        let randomX:Int = (randomSource?.nextInt(upperBound: 614))! - 307
        self.position.x = CGFloat(randomX)
        self.dy = CGFloat((randomSource?.nextUniform())! * 5.0) + 5.0
        self.dx = CGFloat((randomSource?.nextUniform())! * -4.0) + 2.0
        self.isColliding = false
    }
    
    override func Start()
    {
        self.zPosition = 3
        self.Reset()
        self.alpha = 0.5
    }
    
    func updateWithViewOrientation(_ isLandscape: Bool, viewFrame : CGRect?) {
        
        
           guard let frame = viewFrame else {return}
           if isLandscape {
            
           // self.position.y -= viewFrame!.height/2//self.dx!
            self.position.x -=  dy!
            self.position.y -= 1

            
            let max = frame.width/2
            
            if(self.position.x < -1 * max) // note maybe reset earlier
            {
                
                
                self.position.x = frame.width/2
                
                let min = -1 * frame.height/2
                let max = frame.height/2

                let randomY:Int =  Int.random(in: Int(min) ..< Int(max))//(randomSource?.nextInt(upperBound: Int(frame.height)/2))!
                self.position.y = CGFloat(randomY)
                self.isColliding = false

                
                return
                let randomX:Int = (randomSource?.nextInt(upperBound: Int(frame.height)/2))!
                self.position.y = CGFloat(randomX)
                self.dy = CGFloat((randomSource?.nextUniform())! * 5.0) + 5.0
                self.dx = CGFloat((randomSource?.nextUniform())! * -4.0) + 2.0
                self.isColliding = false
                
            }
              

           } else {
            
            self.position.y -= self.dy!
            self.position.x -= self.dx!
            
               if(self.position.y <= -801) // note maybe reset earlier
               {
                   Reset()
                   
               }

           }
          // CheckBounds()
       }
    
    override func Update()
    {
        self.Move()
        self.CheckBounds()
    }
    
    
}
