//
//  Created by giovanne emiliano
//  Student ID: 301044051
//  Copyright © 2020 Centennial College. All rights reserved.
//

import UIKit
import AVFoundation
import SpriteKit
import GameplayKit


class EndScene: SKScene {
    
    
    var oceanSprite1: Ocean?
    var oceanSprite2: Ocean?
    
    var isLandscape = false

    
    override func didMove(to view: SKView)
    {
        screenWidth = frame.width
        screenHeight = frame.height

        self.name = "END"
        
        // add ocean
    
        self.oceanSprite1 = Ocean()
        self.addChild(oceanSprite1!)
        
        self.oceanSprite2 = Ocean()
        self.addChild(oceanSprite2!)
        
        isLandscape = UIDevice.current.orientation.isLandscape
        
        startWithViewOrientation()
              
             
        
    }
    
  func startWithViewOrientation(){
           
           if isLandscape {
               print("isLandscape")
               
               
           
               self.oceanSprite1?.zRotation = -1 * (.pi / 2)
               self.oceanSprite2?.zRotation = -1 * (.pi / 2)

             

               self.oceanSprite1?.position = CGPoint(x:  -1 * (self.frame.width/2) + 325, y: 0)
               self.oceanSprite2?.position = CGPoint(x: self.frame.width, y: 177)
        


           } else {
               print("isPortrait")
           

               self.oceanSprite1?.zRotation = 0
               self.oceanSprite2?.zRotation = 0
               
           
               self.oceanSprite1?.position = CGPoint(x: 0, y: 1864.67)//self.frame.height)
               self.oceanSprite2?.position = CGPoint(x: 0, y: 177)

           }
           
           
           
       }
       
       
       func rotated() {
           
         
          // print("width: \(view?.frame.width) ocen1 x: \(oceanSprite1?.position.x)")
          let t  = UIDevice.current.orientation.isLandscape
           
           if t == isLandscape {
               // print("didnt change")
           } else {
               isLandscape = t
               print("change")
               startWithViewOrientation()
           }
           
         }
       
     
       
    
       
       func touchDown(atPoint pos : CGPoint)
       {
           
       }
       
       func touchMoved(toPoint pos : CGPoint)
       {
           
       }
       
       func touchUp(atPoint pos : CGPoint)
       {
           
       }
       
       override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
       {
           for t in touches { self.touchDown(atPoint: t.location(in: self))}
           
           /*
           if let view = self.view
           {
               if let scene = SKScene(fileNamed: "GameScene")
               {
                   scene.scaleMode = .aspectFill
                   view.presentScene(scene)
               }
           }
            */
       }
       
       override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
       {
           for t in touches { self.touchMoved(toPoint: t.location(in: self))}
       }
       
       override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
       {
           for t in touches { self.touchUp(atPoint: t.location(in: self))}
       }
       
       override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?)
       {
           for t in touches { self.touchUp(atPoint: t.location(in: self))}
       }
       
       
       override func update(_ currentTime: TimeInterval)
       {
            self.oceanSprite1?.updateWithViewOrientation(isLandscape,viewFrame:  self.view?.frame)
            self.oceanSprite2?.updateWithViewOrientation(isLandscape,viewFrame:  self.view?.frame)
                
           
           rotated()
       
       }
}

